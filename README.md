## Push an existing Git repository with the following command:
````
cd existing_repo
git remote add origin https://gitlab.com/lesliediano/test.git
git branch -M main
git push -uf origin main
````

## Add a file using the command line:
````
git status
git add .
git commit -m "commit description"
git push origin main
````

## Other commands:
````
git log
git pull
git checkout
git reset --soft HEAD~1
git revert <commit ID>
````